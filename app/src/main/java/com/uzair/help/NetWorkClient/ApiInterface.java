package com.uzair.help.NetWorkClient;

import com.uzair.help.NetWorkClient.ApisRespons.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("user/login")
    Call<LoginResponse> getUserLoginResponse(@Field("email") String email,
                                             @Field("mobile") String mobile,
                                             @Field("password") String password);
//    Stories

//    @Multipart
//    @POST("auth/story/create")
//    Call<CreateStoryResponse> getCreateStoryResponse(@Part("token") RequestBody token,
//                                                     @Part("privacy") RequestBody privacy,
//                                                     @Part("location") RequestBody location,
//                                                     @Part("latitude") RequestBody latitude,
//                                                     @Part("longitude") RequestBody longitude,
//                                                     @Part MultipartBody.Part image);
//
//
////    Posts
//
//    @FormUrlEncoded
//    @POST("auth/post/wall-posts")
//    Call<AllPostsResponse> getAllPostsResponse (@Field("token") String token);


}
