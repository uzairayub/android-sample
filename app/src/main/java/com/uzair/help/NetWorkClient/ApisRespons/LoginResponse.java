package com.uzair.help.NetWorkClient.ApisRespons;

import com.google.gson.annotations.SerializedName;
import com.uzair.help.Models.ModelUser.UserData;

public class LoginResponse{

	@SerializedName("data")
	private UserData userData;

	@SerializedName("success")
	private boolean success;

	@SerializedName("message")
	private String message;

	public void setUserData(UserData userData){
		this.userData = userData;
	}

	public UserData getUserData(){
		return userData;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"LoginResponse{" + 
			"userData = '" + userData + '\'' +
			",success = '" + success + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}