package com.uzair.help;

import android.support.multidex.MultiDexApplication;

import com.uzair.help.NetWorkClient.ApiClient;
import com.uzair.help.NetWorkClient.ApiInterface;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;


public class AppController extends MultiDexApplication {
    private static AppController mInstance;
    private static ApiInterface apiService;
    private static DB snappy = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        configRetrofit();
    }

    private static void configRetrofit() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public static ApiInterface getApiService() {
        return apiService;
    }


    public static synchronized DB getSnappyInstance() {
        try {
            if (snappy == null) {
                snappy = DBFactory.open(mInstance);
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        return snappy;
    }

}
