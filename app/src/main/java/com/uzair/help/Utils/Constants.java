package com.uzair.help.Utils;

public interface Constants {
    String BASE_URL = "https://ticktoss.com/api/";
    String kStringNetworkConnectivityError = "Please make sure your device is connected with internet.";
    String EXCEPTION = "Exception";
    String EXCEPTION_MESSAGE = "Something went wrong";
    String SERVER_EXCEPTION_MESSAGE = "Something went wrong, server not responding";

    String mFIELD = "fields";
    String mPARAMETERS = "id,name,email,gender,birthday,picture.type(large),cover,age_range";

    // Snappy save User data
    String USER_DATA = "userData";

    // Snappy save my stories data
    String STORY_DATA = "myStoriesData";

    // Preferences
    String PREF_IS_USER_LOGIN = "isUserLogIn";

    //Constants Extras
    String USER_NMAE = "name";
    String USER_EMAIL = "email";
    String USER_GENDER = "gender";
    String USER_ID = "id";
    String USER_AUTH_METHOD = "authMethod";
    String USER_ACCESS_TOKEN = "accessToken";
}
