package com.uzair.help.Models.ModelUser;

import com.google.gson.annotations.SerializedName;

public class TranslationsItem{

	@SerializedName("language")
	private String language;

	@SerializedName("_id")
	private String id;

	@SerializedName("title")
	private String title;

	public void setLanguage(String language){
		this.language = language;
	}

	public String getLanguage(){
		return language;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	@Override
 	public String toString(){
		return 
			"TranslationsItem{" + 
			"language = '" + language + '\'' + 
			",_id = '" + id + '\'' + 
			",title = '" + title + '\'' + 
			"}";
		}
}