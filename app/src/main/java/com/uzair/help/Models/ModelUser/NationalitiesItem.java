package com.uzair.help.Models.ModelUser;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NationalitiesItem{

	@SerializedName("createdAt")
	private String createdAt;

	@SerializedName("translations")
	private List<TranslationsItem> translations;

	@SerializedName("__v")
	private int V;

	@SerializedName("_id")
	private String id;

	@SerializedName("updatedAt")
	private String updatedAt;

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setTranslations(List<TranslationsItem> translations){
		this.translations = translations;
	}

	public List<TranslationsItem> getTranslations(){
		return translations;
	}

	public void setV(int V){
		this.V = V;
	}

	public int getV(){
		return V;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	@Override
 	public String toString(){
		return 
			"NationalitiesItem{" + 
			"createdAt = '" + createdAt + '\'' + 
			",translations = '" + translations + '\'' + 
			",__v = '" + V + '\'' + 
			",_id = '" + id + '\'' + 
			",updatedAt = '" + updatedAt + '\'' + 
			"}";
		}
}