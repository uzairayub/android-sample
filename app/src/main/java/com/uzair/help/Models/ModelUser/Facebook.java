package com.uzair.help.Models.ModelUser;

import com.google.gson.annotations.SerializedName;

public class Facebook{

	@SerializedName("id")
	private String id;

	@SerializedName("accessToken")
	private String accessToken;

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setAccessToken(String accessToken){
		this.accessToken = accessToken;
	}

	public String getAccessToken(){
		return accessToken;
	}

	@Override
 	public String toString(){
		return 
			"Facebook{" + 
			"id = '" + id + '\'' + 
			",accessToken = '" + accessToken + '\'' + 
			"}";
		}
}