package com.uzair.help.Models.ModelUser;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserData {

	@SerializedName("lastSeenAt")
	private String lastSeenAt;

	@SerializedName("bodyType")
	private BodyType bodyType;

	@SerializedName("country")
	private Country country;

	@SerializedName("occupation")
	private String occupation;

	@SerializedName("education")
	private String education;

	@SerializedName("hasPrivateLocker")
	private boolean hasPrivateLocker;

	@SerializedName("skinColor")
	private SkinColor skinColor;

	@SerializedName("watchedFollowingsStories")
	private List<Object> watchedFollowingsStories;

	@SerializedName("about")
	private String about;

	@SerializedName("mobileVerified")
	private boolean mobileVerified;

	@SerializedName("nationalities")
	private List<NationalitiesItem> nationalities;

	@SerializedName("language")
	private Language language;

	@SerializedName("bitmoji")
	private String bitmoji;

	@SerializedName("instagram")
	private Instagram instagram;

	@SerializedName("photos")
	private List<PhotosItem> photos;

	@SerializedName("createdAt")
	private String createdAt;

	@SerializedName("profilePhoto")
	private String profilePhoto;

	@SerializedName("school")
	private String school;

	@SerializedName("neighbourhood")
	private Neighbourhood neighbourhood;

	@SerializedName("twoFactorAuthentication")
	private boolean twoFactorAuthentication;

	@SerializedName("storiesForFollowers")
	private List<Object> storiesForFollowers;

	@SerializedName("hivStatus")
	private String hivStatus;

	@SerializedName("longitude")
	private double longitude;

	@SerializedName("height")
	private int height;

	@SerializedName("updatedAt")
	private String updatedAt;

	@SerializedName("accountType")
	private String accountType;

	@SerializedName("reactedPosts")
	private List<Object> reactedPosts;

	@SerializedName("firstName")
	private String firstName;

	@SerializedName("autoFollowAfter")
	private int autoFollowAfter;

	@SerializedName("autoSwipeAfter")
	private int autoSwipeAfter;

	@SerializedName("_id")
	private String id;

	@SerializedName("maritalStatus")
	private MaritalStatus maritalStatus;

	@SerializedName("income")
	private int income;

	@SerializedName("lastName")
	private String lastName;

	@SerializedName("gender")
	private String gender;

	@SerializedName("ethnicity")
	private Ethnicity ethnicity;

	@SerializedName("purpose")
	private Purpose purpose;

	@SerializedName("city")
	private City city;

	@SerializedName("latitude")
	private double latitude;

	@SerializedName("linkedin")
	private Linkedin linkedin;

	@SerializedName("reactedStories")
	private List<Object> reactedStories;

	@SerializedName("mobileVerificationCode")
	private String mobileVerificationCode;

	@SerializedName("email")
	private String email;

	@SerializedName("storiesForMatches")
	private List<Object> storiesForMatches;

	@SerializedName("orientation")
	private Orientation orientation;

	@SerializedName("address")
	private String address;

	@SerializedName("storiesForFriends")
	private List<Object> storiesForFriends;

	@SerializedName("dailyFollowLimit")
	private int dailyFollowLimit;

	@SerializedName("facebook")
	private Facebook facebook;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("coverPhoto")
	private String coverPhoto;

	@SerializedName("fullName")
	private String fullName;

	@SerializedName("userName")
	private String userName;

	@SerializedName("watchedMatchesStories")
	private List<Object> watchedMatchesStories;

	@SerializedName("dailySwipeLimit")
	private int dailySwipeLimit;

	@SerializedName("religion")
	private Religion religion;

	@SerializedName("token")
	private String token;

	@SerializedName("emailVerified")
	private boolean emailVerified;

	@SerializedName("watchedFriendsStories")
	private List<Object> watchedFriendsStories;

	@SerializedName("interests")
	private List<InterestsItem> interests;

	public void setLastSeenAt(String lastSeenAt){
		this.lastSeenAt = lastSeenAt;
	}

	public String getLastSeenAt(){
		return lastSeenAt;
	}

	public void setBodyType(BodyType bodyType){
		this.bodyType = bodyType;
	}

	public BodyType getBodyType(){
		return bodyType;
	}

	public void setCountry(Country country){
		this.country = country;
	}

	public Country getCountry(){
		return country;
	}

	public void setOccupation(String occupation){
		this.occupation = occupation;
	}

	public String getOccupation(){
		return occupation;
	}

	public void setEducation(String education){
		this.education = education;
	}

	public String getEducation(){
		return education;
	}

	public void setHasPrivateLocker(boolean hasPrivateLocker){
		this.hasPrivateLocker = hasPrivateLocker;
	}

	public boolean isHasPrivateLocker(){
		return hasPrivateLocker;
	}

	public void setSkinColor(SkinColor skinColor){
		this.skinColor = skinColor;
	}

	public SkinColor getSkinColor(){
		return skinColor;
	}

	public void setWatchedFollowingsStories(List<Object> watchedFollowingsStories){
		this.watchedFollowingsStories = watchedFollowingsStories;
	}

	public List<Object> getWatchedFollowingsStories(){
		return watchedFollowingsStories;
	}

	public void setAbout(String about){
		this.about = about;
	}

	public String getAbout(){
		return about;
	}

	public void setMobileVerified(boolean mobileVerified){
		this.mobileVerified = mobileVerified;
	}

	public boolean isMobileVerified(){
		return mobileVerified;
	}

	public void setNationalities(List<NationalitiesItem> nationalities){
		this.nationalities = nationalities;
	}

	public List<NationalitiesItem> getNationalities(){
		return nationalities;
	}

	public void setLanguage(Language language){
		this.language = language;
	}

	public Language getLanguage(){
		return language;
	}

	public void setBitmoji(String bitmoji){
		this.bitmoji = bitmoji;
	}

	public String getBitmoji(){
		return bitmoji;
	}

	public void setInstagram(Instagram instagram){
		this.instagram = instagram;
	}

	public Instagram getInstagram(){
		return instagram;
	}

	public void setPhotos(List<PhotosItem> photos){
		this.photos = photos;
	}

	public List<PhotosItem> getPhotos(){
		return photos;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setProfilePhoto(String profilePhoto){
		this.profilePhoto = profilePhoto;
	}

	public String getProfilePhoto(){
		return profilePhoto;
	}

	public void setSchool(String school){
		this.school = school;
	}

	public String getSchool(){
		return school;
	}

	public void setNeighbourhood(Neighbourhood neighbourhood){
		this.neighbourhood = neighbourhood;
	}

	public Neighbourhood getNeighbourhood(){
		return neighbourhood;
	}

	public void setTwoFactorAuthentication(boolean twoFactorAuthentication){
		this.twoFactorAuthentication = twoFactorAuthentication;
	}

	public boolean isTwoFactorAuthentication(){
		return twoFactorAuthentication;
	}

	public void setStoriesForFollowers(List<Object> storiesForFollowers){
		this.storiesForFollowers = storiesForFollowers;
	}

	public List<Object> getStoriesForFollowers(){
		return storiesForFollowers;
	}

	public void setHivStatus(String hivStatus){
		this.hivStatus = hivStatus;
	}

	public String getHivStatus(){
		return hivStatus;
	}

	public void setLongitude(double longitude){
		this.longitude = longitude;
	}

	public double getLongitude(){
		return longitude;
	}

	public void setHeight(int height){
		this.height = height;
	}

	public int getHeight(){
		return height;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setAccountType(String accountType){
		this.accountType = accountType;
	}

	public String getAccountType(){
		return accountType;
	}

	public void setReactedPosts(List<Object> reactedPosts){
		this.reactedPosts = reactedPosts;
	}

	public List<Object> getReactedPosts(){
		return reactedPosts;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setAutoFollowAfter(int autoFollowAfter){
		this.autoFollowAfter = autoFollowAfter;
	}

	public int getAutoFollowAfter(){
		return autoFollowAfter;
	}

	public void setAutoSwipeAfter(int autoSwipeAfter){
		this.autoSwipeAfter = autoSwipeAfter;
	}

	public int getAutoSwipeAfter(){
		return autoSwipeAfter;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setMaritalStatus(MaritalStatus maritalStatus){
		this.maritalStatus = maritalStatus;
	}

	public MaritalStatus getMaritalStatus(){
		return maritalStatus;
	}

	public void setIncome(int income){
		this.income = income;
	}

	public int getIncome(){
		return income;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setEthnicity(Ethnicity ethnicity){
		this.ethnicity = ethnicity;
	}

	public Ethnicity getEthnicity(){
		return ethnicity;
	}

	public void setPurpose(Purpose purpose){
		this.purpose = purpose;
	}

	public Purpose getPurpose(){
		return purpose;
	}

	public void setCity(City city){
		this.city = city;
	}

	public City getCity(){
		return city;
	}

	public void setLatitude(double latitude){
		this.latitude = latitude;
	}

	public double getLatitude(){
		return latitude;
	}

	public void setLinkedin(Linkedin linkedin){
		this.linkedin = linkedin;
	}

	public Linkedin getLinkedin(){
		return linkedin;
	}

	public void setReactedStories(List<Object> reactedStories){
		this.reactedStories = reactedStories;
	}

	public List<Object> getReactedStories(){
		return reactedStories;
	}

	public void setMobileVerificationCode(String mobileVerificationCode){
		this.mobileVerificationCode = mobileVerificationCode;
	}

	public String getMobileVerificationCode(){
		return mobileVerificationCode;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setStoriesForMatches(List<Object> storiesForMatches){
		this.storiesForMatches = storiesForMatches;
	}

	public List<Object> getStoriesForMatches(){
		return storiesForMatches;
	}

	public void setOrientation(Orientation orientation){
		this.orientation = orientation;
	}

	public Orientation getOrientation(){
		return orientation;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setStoriesForFriends(List<Object> storiesForFriends){
		this.storiesForFriends = storiesForFriends;
	}

	public List<Object> getStoriesForFriends(){
		return storiesForFriends;
	}

	public void setDailyFollowLimit(int dailyFollowLimit){
		this.dailyFollowLimit = dailyFollowLimit;
	}

	public int getDailyFollowLimit(){
		return dailyFollowLimit;
	}

	public void setFacebook(Facebook facebook){
		this.facebook = facebook;
	}

	public Facebook getFacebook(){
		return facebook;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setCoverPhoto(String coverPhoto){
		this.coverPhoto = coverPhoto;
	}

	public String getCoverPhoto(){
		return coverPhoto;
	}

	public void setFullName(String fullName){
		this.fullName = fullName;
	}

	public String getFullName(){
		return fullName;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setWatchedMatchesStories(List<Object> watchedMatchesStories){
		this.watchedMatchesStories = watchedMatchesStories;
	}

	public List<Object> getWatchedMatchesStories(){
		return watchedMatchesStories;
	}

	public void setDailySwipeLimit(int dailySwipeLimit){
		this.dailySwipeLimit = dailySwipeLimit;
	}

	public int getDailySwipeLimit(){
		return dailySwipeLimit;
	}

	public void setReligion(Religion religion){
		this.religion = religion;
	}

	public Religion getReligion(){
		return religion;
	}

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

	public void setEmailVerified(boolean emailVerified){
		this.emailVerified = emailVerified;
	}

	public boolean isEmailVerified(){
		return emailVerified;
	}

	public void setWatchedFriendsStories(List<Object> watchedFriendsStories){
		this.watchedFriendsStories = watchedFriendsStories;
	}

	public List<Object> getWatchedFriendsStories(){
		return watchedFriendsStories;
	}

	public void setInterests(List<InterestsItem> interests){
		this.interests = interests;
	}

	public List<InterestsItem> getInterests(){
		return interests;
	}

	@Override
 	public String toString(){
		return 
			"UserData{" +
			"lastSeenAt = '" + lastSeenAt + '\'' + 
			",bodyType = '" + bodyType + '\'' + 
			",country = '" + country + '\'' + 
			",occupation = '" + occupation + '\'' + 
			",education = '" + education + '\'' + 
			",hasPrivateLocker = '" + hasPrivateLocker + '\'' + 
			",skinColor = '" + skinColor + '\'' + 
			",watchedFollowingsStories = '" + watchedFollowingsStories + '\'' + 
			",about = '" + about + '\'' + 
			",mobileVerified = '" + mobileVerified + '\'' + 
			",nationalities = '" + nationalities + '\'' + 
			",language = '" + language + '\'' + 
			",bitmoji = '" + bitmoji + '\'' + 
			",instagram = '" + instagram + '\'' + 
			",photos = '" + photos + '\'' + 
			",createdAt = '" + createdAt + '\'' + 
			",profilePhoto = '" + profilePhoto + '\'' + 
			",school = '" + school + '\'' + 
			",neighbourhood = '" + neighbourhood + '\'' + 
			",twoFactorAuthentication = '" + twoFactorAuthentication + '\'' + 
			",storiesForFollowers = '" + storiesForFollowers + '\'' + 
			",hivStatus = '" + hivStatus + '\'' + 
			",longitude = '" + longitude + '\'' + 
			",height = '" + height + '\'' + 
			",updatedAt = '" + updatedAt + '\'' + 
			",accountType = '" + accountType + '\'' + 
			",reactedPosts = '" + reactedPosts + '\'' + 
			",firstName = '" + firstName + '\'' + 
			",autoFollowAfter = '" + autoFollowAfter + '\'' + 
			",autoSwipeAfter = '" + autoSwipeAfter + '\'' + 
			",_id = '" + id + '\'' + 
			",maritalStatus = '" + maritalStatus + '\'' + 
			",income = '" + income + '\'' + 
			",lastName = '" + lastName + '\'' + 
			",gender = '" + gender + '\'' + 
			",ethnicity = '" + ethnicity + '\'' + 
			",purpose = '" + purpose + '\'' + 
			",city = '" + city + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",linkedin = '" + linkedin + '\'' + 
			",reactedStories = '" + reactedStories + '\'' + 
			",mobileVerificationCode = '" + mobileVerificationCode + '\'' + 
			",email = '" + email + '\'' + 
			",storiesForMatches = '" + storiesForMatches + '\'' + 
			",orientation = '" + orientation + '\'' + 
			",address = '" + address + '\'' + 
			",storiesForFriends = '" + storiesForFriends + '\'' + 
			",dailyFollowLimit = '" + dailyFollowLimit + '\'' + 
			",facebook = '" + facebook + '\'' + 
			",mobile = '" + mobile + '\'' + 
			",coverPhoto = '" + coverPhoto + '\'' + 
			",fullName = '" + fullName + '\'' + 
			",userName = '" + userName + '\'' + 
			",watchedMatchesStories = '" + watchedMatchesStories + '\'' + 
			",dailySwipeLimit = '" + dailySwipeLimit + '\'' + 
			",religion = '" + religion + '\'' + 
			",token = '" + token + '\'' + 
			",emailVerified = '" + emailVerified + '\'' + 
			",watchedFriendsStories = '" + watchedFriendsStories + '\'' + 
			",interests = '" + interests + '\'' + 
			"}";
		}
}