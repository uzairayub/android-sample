package com.uzair.help.Models.ModelUser;

import com.google.gson.annotations.SerializedName;

public class PhotosItem{

	@SerializedName("image")
	private String image;

	@SerializedName("__v")
	private int V;

	@SerializedName("_id")
	private String id;

	@SerializedName("user")
	private String user;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setV(int V){
		this.V = V;
	}

	public int getV(){
		return V;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setUser(String user){
		this.user = user;
	}

	public String getUser(){
		return user;
	}

	@Override
 	public String toString(){
		return 
			"PhotosItem{" + 
			"image = '" + image + '\'' + 
			",__v = '" + V + '\'' + 
			",_id = '" + id + '\'' + 
			",user = '" + user + '\'' + 
			"}";
		}
}