package com.uzair.help.Models.ModelUser;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Neighbourhood{

	@SerializedName("createdAt")
	private String createdAt;

	@SerializedName("city")
	private String city;

	@SerializedName("translations")
	private List<TranslationsItem> translations;

	@SerializedName("__v")
	private int V;

	@SerializedName("_id")
	private String id;

	@SerializedName("updatedAt")
	private String updatedAt;

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setTranslations(List<TranslationsItem> translations){
		this.translations = translations;
	}

	public List<TranslationsItem> getTranslations(){
		return translations;
	}

	public void setV(int V){
		this.V = V;
	}

	public int getV(){
		return V;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	@Override
 	public String toString(){
		return 
			"Neighbourhood{" + 
			"createdAt = '" + createdAt + '\'' + 
			",city = '" + city + '\'' + 
			",translations = '" + translations + '\'' + 
			",__v = '" + V + '\'' + 
			",_id = '" + id + '\'' + 
			",updatedAt = '" + updatedAt + '\'' + 
			"}";
		}
}