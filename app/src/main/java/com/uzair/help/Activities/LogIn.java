package com.uzair.help.Activities;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.facebook.login.Login;
import com.uzair.help.AppController;
import com.uzair.help.NetWorkClient.ApisRespons.LoginResponse;
import com.uzair.help.R;
import com.uzair.help.Utils.Constants;
import com.uzair.help.Utils.SnappyDBUtil;
import com.uzair.help.Utils.Util;
import com.uzair.help.Utils.Utilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogIn extends AppCompatActivity implements View.OnClickListener {

    private EditText etEmail, etPassword;
    private String email, mobile, password, error;
    private Button btnLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        initial();

    }

    private void initial()
    {
        etEmail = (EditText)findViewById(R.id.login_et_email);
        etPassword = (EditText)findViewById(R.id.login_et_password);
        btnLogin = (Button) findViewById(R.id.login_btn_login);
        btnLogin.setOnClickListener(this);

    }

    @Override
    public void onClick(View view)
    {
        if(view.getId() == btnLogin.getId())
        {
            if(validation())
            {
                if(isEmail())
                {
                    loginUser();
                }
            }
        }
    }

    private void loginUser()
    {
        if (!Util.isConnectingToInternet(this))
        {
            Util.showToastMsg(this, Constants.kStringNetworkConnectivityError);
            return;
        }
        final AlertDialog dialog = Util.progressDialog(LogIn.this);
        dialog.show();
        Call<LoginResponse> call = AppController.getApiService().getUserLoginResponse(email,mobile,password);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                dialog.dismiss();
                try
                {
                    if (response.body().isSuccess())
                    {
                        Util.showToastMsg(getApplicationContext(), "User login successfully");
                        SnappyDBUtil.saveObject(Constants.USER_DATA,response.body().getUserData());
                        Utilities.getInstance(getApplicationContext()).saveBooleanPreferences(Constants.PREF_IS_USER_LOGIN, true);
                    }
                    else
                    {
                        Util.showToastMsg(getApplicationContext(), response.body().getMessage());
                    }
                } catch (Exception e) {
                    Util.showToastMsg(getApplicationContext(), Constants.EXCEPTION_MESSAGE);
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t)
            {
                dialog.dismiss();
                Log.e(Constants.EXCEPTION, t.getMessage());
                Util.showToastMsg(getApplicationContext(), Constants.SERVER_EXCEPTION_MESSAGE);
            }
        });
    }

    private boolean validation()
    {
        email = etEmail.getText().toString().trim();
        password = etPassword.getText().toString().trim();

        if(email.isEmpty())
        {
            error = getResources().getString(R.string.erroremail);
            etEmail.setError(error);
            return false;
        }
        if(password.isEmpty())
        {
            error = getResources().getString(R.string.errorpassword);
            etPassword.setError(error);
            return false;
        }
        return true;

    }

    private boolean isEmail()
    {
        if(Util.isValidEmail(email))
        {
            mobile = null;
            return true;
        }
        if(Util.isValidMobile(email))
        {
            mobile = email;
            email = null;
            return true;
        }
        error = getResources().getString(R.string.erroremail);
        etEmail.setError(error);
        return false;
    }
}
